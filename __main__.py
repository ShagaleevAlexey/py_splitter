import os
import argparse

parser = argparse.ArgumentParser(description='Split text file by line')
parser.add_argument('filepath', metavar='filepath', type=str, nargs='+', default='addresses.txt', help='path to file need for split')
parser.add_argument('path', metavar='path', type=str, nargs='+', default='split', help='path to result path')
parser.add_argument('count', metavar='count', type=int, nargs='+', default=10, help='count results path')
parser.add_argument('-prefix', metavar='prefix', type=str, nargs='+', default=[''], help='prefix for result files')

args = parser.parse_args()

filepath = args.filepath[0]
path = args.path[0]
count = args.count[0]
prefix = args.prefix[0]

assert os.path.exists(filepath), f'File \'{filepath}\' not found'

if not os.path.exists(path):
    os.makedirs(path)

count_lines = 0
index = 0
files = 0

def get_file_name(number):
    basefile = path

    snumber = str(number)

    if isinstance(prefix, str):
        snumber = prefix + snumber

    return os.path.join(basefile, snumber) + '.txt'

with open(filepath, encoding='ISO-8859-1') as basefile:
    while basefile.readline():
        count_lines += 1

    basefile.seek(0)

    file = open(get_file_name(files), 'w')

    line = basefile.readline()

    while line:
        index += 1

        file.write(line)

        if index >= count_lines / count:
            index = 0
            files += 1

            file.close()

            if files < count:
                file = open(get_file_name(files), 'w')

        line = basefile.readline()

try:
    file.close()
except:
    pass

pass
